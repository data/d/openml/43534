# OpenML dataset: Production-cross-sections-of-Inert-Doublet-Model

https://www.openml.org/d/43534

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
'Learning the production cross-sections of the Inert Doublet Model'

Cite as  
Humberto Reyes-Gonzlez, Andre Lessa,  Sydney Otten. (2020). 'Learning the production cross sections of the Inert Doublet Model' training data set. [Data set]. Zenodo. http://doi.org/10.5281/zenodo.3689678
Content
Pheno AI training dataset used in the ''Learning the production cross-sections of the Inert Doublet Model'' subproject, made of 50000 samples with 
5 input values: 

MH0
MA0
MHC 
lam2
lamL 

and 
8 target values 

xsec353513TeV
xsec363613TeV
xsec373713TeV
xsec353713TeV
xsec363713TeV
xsec373513TeV
xsec373613TeV
xsec353613TeV 

from a parameter space of the Inert Doublet Model chosen as: 50 MH0, MA0, MHC3000GeV; 2  lam2, lamL2. 
The cross-sections were computed at leading order using MADGRAPH2.6.4 and the IDM UFO implementation from the FeynRules database.
Inert Doublet Model
The inert doublet model, a minimal extension of the Standard Model by a second higgs doublet with no direct couplings to quarks or leptons, is one of the simplest scenarios that can explain the dark matter.
Additional information
Pheno AI training data   
Les Houches project for a database of networks for regression and classification of quantities relevant for particle physics phenomenology.
Curated by: scaron123  
Curation policy: Contact organisers of the Les Houches project  
Created: June 25, 2019  
Harvesting API: OAI-PMH Interface  
Acknowledgements

darkmachines.org
phenomldata.org

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43534) of an [OpenML dataset](https://www.openml.org/d/43534). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43534/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43534/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43534/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

